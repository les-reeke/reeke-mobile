# Presentation

Reeke Mobile app.



# Environement de développement

Android Studio version : 3.6.3  
Kotlin version : 1.3.7-release-studio3.6.1  
Android sdk version (minimum) : 23



# Prérequis

Récupérer le projet localement  
    `git submodule foreach git pull origin develop`  
    `git submodule foreach git pull -r`  


S'assurer d'être sur la branch develop  
    `git branch`  
    Sinon :  
    `git checkout develop`  
   
   
    
# Procédure (dev)

Lancer Android Studio
Ouvrir le projet : File > Open > [Chemin vers le répertoire]

Avec une virtual device :
- Configurer une "Virtual Device" :  
    -> Tools > AVD Manager (https://developer.android.com/studio/run/managing-avds)
- Selectionner la "Virtual Device" (en haut à droite menu déroulant. Si aucune device, affiche "No device")
- Lancer l'application (petit triangle vert en haut à droite, à côté de la selection du "device") OU Run > Run 'app'
    
Avec un smartphone :
- Brancher en USB le smartphone
- Sur le smartphone :  
    -> Activer le mode développeur (https://www.frandroid.com/comment-faire/tutoriaux/184906_comment-acceder-au-mode-developpeur-sur-android)  
    -> Activer le débogage (dans les options pour développeur)
- Sur Android Studio :  
    -> Selectionner le smartphone (en haut à droite menu déroulant. Si aucune device, affiche "No device")  
    -> Lancer l'application (petit triangle vert en haut à droite, à côté de la selection du "device") OU Run > Run 'app'  
       
        
En cas de problème essayez la prcédure suivante :
- Build > Clean Project
- Build > Rebuild Project