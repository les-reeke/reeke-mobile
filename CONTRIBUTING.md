# Commits

## Message du commit
```
<type>(<scope>): <sujet>
<LIGNE VIDE>
<corps>
<LIGNE VIDE>
<footer>
```

## Type
* **build**: Changements affectant uniquement la compilation/dépendances
* **ci**: Changement affectant le CI/CD
* **docs**: Documentation
* **feat**: Nouvelles fonctionnalitées
* **fix**: Correction de bugs
* **perf**: Amélioration des performances
* **refactor**: Refacto du code qui affecte uniquement son style

## Sujet
Brève description des modifications

## Body
Expliquer en détail vos modifications.

## Footer
Utiliser le footer pour les tags liés à Gitlab (ex: fix #1) ou liés aux CI/CD jobs.
