package com.example.lesreekemobile

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.util.Log
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONException
import org.json.JSONObject
import java.lang.StringBuilder
import java.io.BufferedReader
import java.io.BufferedWriter
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL


/**
 * L'URL ne doit pas se terminer par /
 */
class Client(context: Context, url: String) {

    val urlServ = url
    val myContext = context

    fun checkNetworkConnection(): Boolean {
        val connMgr = myContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val n = connMgr.activeNetwork
        if (n != null) {
            val nc = connMgr.getNetworkCapabilities(n)
            val networkInfo = nc.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) || nc.hasTransport(
                NetworkCapabilities.TRANSPORT_WIFI
            )

            val isConnected: Boolean = networkInfo
            if (isConnected) {
                // show "Connected" & type of network "WIFI or MOBILE"
                val connectionStatus = "Connected $networkInfo"
            } else {
                // show "Not Connected"
                val connectionStatust = "Not Connected"
            }
            return isConnected
        }
        return false
    }

    /**
     * L'URL doit commencer par /
     */
    suspend fun httpRequest(method: String, endpoint: String): String {

        val result = withContext(Dispatchers.IO) {

            val myUrl = urlServ + endpoint
            val url = URL(myUrl)
            // 1. create HttpURLConnection
            val conn = url.openConnection() as HttpURLConnection
            conn.requestMethod = method
            //conn.setRequestProperty("Content-Type", "application/json; charset=utf-8")

//            // 2. build JSON object
//            val jsonObject = parse(sendingDatas)
//
//            // 3. add JSON content to POST request body
//            if (jsonObject != null) {
//                setPostRequestContent(conn, jsonObject)
//            }

            // 4. make POST request to the given URL
            conn.connect()

            // 5. return response message
            if (conn.responseCode in 200..299) {
                var br = BufferedReader(InputStreamReader(conn.inputStream));
                var strCurrentLine: String?
                val strBuilder = StringBuilder()
                while (br.readLine().also { strCurrentLine = it } != null) {
                    strBuilder.append(strCurrentLine + "\n")
                }
                strBuilder.toString()
            } else {
                var br = BufferedReader(InputStreamReader(conn.errorStream))
                print(br.readLine())
                br.readLine()
            }
        }
        return result
    }

    /**
     * L'URL doit commencer par /
     */
    suspend fun httpRequest(method: String, endpoint: String, sendingDatas: ArrayList<ArrayList<String>>): String {

        val result = withContext(Dispatchers.IO) {

            val myUrl = urlServ + endpoint
            val url = URL(myUrl)
            // 1. create HttpURLConnection
            val conn = url.openConnection() as HttpURLConnection
            conn.requestMethod = method
            //conn.setRequestProperty("Content-Type", "application/json; charset=utf-8")
            sendingDatas.forEach {
                conn.setRequestProperty(it[0], it[1])
            }

//            // 2. build JSON object
//            val jsonObject = parse(sendingDatas)
//
//            // 3. add JSON content to POST request body
//            if (jsonObject != null) {
//                setPostRequestContent(conn, jsonObject)
//            }

            // 4. make POST request to the given URL
            conn.connect()

            // 5. return response message
            if (conn.responseCode in 200..299) {
                var br = BufferedReader(InputStreamReader(conn.inputStream));
                var strCurrentLine: String?
                val strBuilder = StringBuilder()
                while (br.readLine().also { strCurrentLine = it } != null) {
                    strBuilder.append(strCurrentLine + "\n")
                }
                strBuilder.toString()
            } else {
                var br = BufferedReader(InputStreamReader(conn.errorStream))
                print(br.readLine())
                br.readLine()
            }
        }
        return result
    }

    private fun setPostRequestContent(conn: HttpURLConnection, jsonObject: JSONObject) {

        val os = conn.outputStream
        val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
        writer.write(jsonObject.toString())
        Log.i(MainActivity::class.java.toString(), jsonObject.toString())
        writer.flush()
        writer.close()
        os.close()
    }

    fun parse(json: String): JSONObject? {
        var jsonObject: JSONObject? = null
        try {
            jsonObject = JSONObject(json)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        return jsonObject
    }
}