package com.example.lesreekemobile.`class`

import java.io.Serializable

data class Restau(var name: String, var country: String, var tel: String, var mail: String, var address: String, var latitude: Double, var longitude: Double ) : Serializable