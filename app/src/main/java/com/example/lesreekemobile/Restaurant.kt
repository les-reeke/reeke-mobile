package com.example.lesreekemobile

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.example.lesreekemobile.`class`.Restau

class Restaurant : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_restaurant)
        val titreRestau : TextView = findViewById(R.id.textViewRestaurent)
        val monRestau = intent.getSerializableExtra(intent.getStringExtra("name")) as Restau
        titreRestau.setText(monRestau.name)
        val fragmentTransaction: FragmentTransaction = supportFragmentManager.beginTransaction()
        val horaire : Fragment = RestauHoraire()
        val contact : Fragment = restau_contact()
        val menu : Fragment = RestauMenu()
        fragmentTransaction.add(R.id.fragment_container, horaire)
        fragmentTransaction.commit()

        val btnMenu : Button = findViewById(R.id.btnMenu)
        val btnHoraires : Button = findViewById(R.id.btnHoraires)
        val btnContact : Button = findViewById(R.id.btnContact)

        btnHoraires.setBackgroundResource(R.drawable.selected_button)
        btnHoraires.setTextColor(resources.getColor(R.color.colorWhite))

        btnHoraires.setOnClickListener {
            btnMenu.setBackgroundResource(R.drawable.custom_button)
            btnMenu.setTextColor(resources.getColor(R.color.colorBlack))

            btnContact.setBackgroundResource(R.drawable.custom_button)
            btnContact.setTextColor(resources.getColor(R.color.colorBlack))

            btnHoraires.setBackgroundResource(R.drawable.selected_button)
            btnHoraires.setTextColor(resources.getColor(R.color.colorWhite))

            val fr : FragmentTransaction? = supportFragmentManager.beginTransaction()
            fr!!.replace(R.id.fragment_container, horaire)
            fr.commit()
        }
        btnContact.setOnClickListener {
            btnMenu.setBackgroundResource(R.drawable.custom_button)
            btnMenu.setTextColor(resources.getColor(R.color.colorBlack))

            btnContact.setBackgroundResource(R.drawable.selected_button)
            btnContact.setTextColor(resources.getColor(R.color.colorWhite))

            btnHoraires.setBackgroundResource(R.drawable.custom_button)
            btnHoraires.setTextColor(resources.getColor(R.color.colorBlack))
            val args: Bundle = Bundle()
            args.putString("tel", monRestau.tel)
            args.putString("mail", monRestau.mail)
            args.putString("address", monRestau.address)
            contact.arguments = args
            val fr : FragmentTransaction? = supportFragmentManager.beginTransaction()
            val b = fr!!.replace(R.id.fragment_container, contact)
            fr.commit()
        }
        btnMenu.setOnClickListener {
            btnMenu.setBackgroundResource(R.drawable.selected_button)
            btnMenu.setTextColor(resources.getColor(R.color.colorWhite))

            btnContact.setBackgroundResource(R.drawable.custom_button)
            btnContact.setTextColor(resources.getColor(R.color.colorBlack))

            btnHoraires.setBackgroundResource(R.drawable.custom_button)
            btnHoraires.setTextColor(resources.getColor(R.color.colorBlack))

            val fr : FragmentTransaction? = supportFragmentManager.beginTransaction()
            fr!!.replace(R.id.fragment_container, menu)
            fr.commit()
        }

        // get reference to ImageView
        val addFav = findViewById<ImageView>(R.id.addToFav)
        var fav = false
        // set on-click listener
        addFav.setOnClickListener {
            if (!fav) {
                addFav.setImageResource(R.drawable.fav2);
                fav = true;
            } else if (fav) {
                addFav.setImageResource(R.drawable.fav1);
                fav = false;
            }
        }

        // get reference to ImageView
        val btnContrib = findViewById<Button>(R.id.btnContrib)
        // set on-click listener
        btnContrib.setOnClickListener {
            val intent = Intent(this, Qualification::class.java)
            startActivity(intent)
        }
    }
}