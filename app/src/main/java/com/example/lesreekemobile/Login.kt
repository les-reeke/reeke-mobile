package com.example.lesreekemobile

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.launch

class Login : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        // get reference to ImageView
        val buttonConnect = findViewById<Button>(R.id.buttonConnect)
        // set on-click listener
        buttonConnect.setOnClickListener {

            val editTextID = findViewById<EditText>(R.id.editTextID)
            val editTextMDP = findViewById<EditText>(R.id.editTextMDP)

            if ( !editTextID.text.isBlank()
                && !editTextMDP.text.isBlank() ) {

                var datas = ArrayList<ArrayList<String>>()

                val id = ArrayList<String>()
                id.add("login")
                id.add(editTextID.text.toString())
                val password = ArrayList<String>()
                password.add("password")
                password.add(editTextMDP.text.toString())

                datas.add(id)
                datas.add(password)
                Log.d("test", datas.toString())

                val client = Client(this, "https://api.partagetesco.fr/v-latest")
                if(client.checkNetworkConnection()) {
                    var result: String?
                    var token: String?
                    var context = this
                    lifecycleScope.launch {
                        result = client.httpRequest("POST", "/user/login", datas)
                        Log.d("RESULT", result)

                        val json = client.parse(result!!)
                        if (json != null) {

                            if (!json.isNull("data")) {
                                val jsonArray = json.getJSONObject("data")
                                val jsonObject = jsonArray.getJSONObject("attributes")
                                token = jsonObject.getString("access_token")

                                val intent = Intent(context, MyAccount::class.java)
                                intent.putExtra("token", token)
                                startActivity(intent)
                            }
                        }
                    }
                }
            }
        }

        // get reference to ImageView
        val buttonINSCRIPTION = findViewById<Button>(R.id.buttonINSCRIPTION)
        // set on-click listener
        buttonINSCRIPTION.setOnClickListener {
            val intent = Intent(this, Inscription::class.java)
            startActivity(intent)
         }
    }
}