package com.example.lesreekemobile

//import android.R
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
/**
 * A simple [Fragment] subclass.
 * Use the [NavbarBottom.newInstance] factory method to
 * create an instance of this fragment.
 */
class NavbarBottom : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // TODO Auto-generated method stub
        val v: View = inflater.inflate(R.layout.fragment_navbar_bottom, container, false)
        toActivityExplorer(v)
        toActivityFavoris(v)
        toActivityFiltres(v)
        toActivityProfil(v)
        return v
    }

    private fun toActivityExplorer(v: View) {
        // get reference to ImageView
        val imageView6 = v.findViewById(R.id.explorer) as ImageView
        // set on-click listener
        imageView6.setOnClickListener {
            val intent = Intent(activity, Map::class.java)
            startActivity(intent)
        }
    }
    private fun toActivityFavoris(v: View) {
        // get reference to ImageView
        val imageView6 = v.findViewById(R.id.favoris) as ImageView
        // set on-click listener
        imageView6.setOnClickListener {
            /*TODO: Mettre la bonne activité*/
            val intent = Intent(activity, Accueil::class.java)
            startActivity(intent)
        }
    }
    private fun toActivityFiltres(v: View) {
        // get reference to ImageView
        val imageView6 = v.findViewById(R.id.filtres) as ImageView
        // set on-click listener
        imageView6.setOnClickListener {
            val intent = Intent(activity, Filters::class.java)
            startActivity(intent)
        }
    }
    private fun toActivityProfil(v: View) {
        // get reference to ImageView
        val imageView6 = v.findViewById(R.id.profil) as ImageView
        // set on-click listener
        imageView6.setOnClickListener {
            /*TODO: Mettre la bonne activité*/
            //ATTENTION : Pour le moment on redirige sur login il faudrat
            // implementer pour detecter si le user et deja co ou non !!!
            val intent = Intent(activity, Login::class.java)
            startActivity(intent)
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment NavbarBottom.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic fun newInstance(param1: String, param2: String) =
            NavbarBottom().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
