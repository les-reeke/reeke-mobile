package com.example.lesreekemobile

import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.launch
import org.osmdroid.views.overlay.OverlayItem

class Inscription : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_inscription)

        selectAllergie()
    }

    private fun selectAllergie() {
        val imageViewALLERGIE = findViewById<ImageView>(R.id.imageViewLIST)
        val textViewLIST = findViewById<TextView>(R.id.textViewLIST)

        val allergene = resources.getStringArray(R.array.allergene);
        var checkedAllergene = BooleanArray(allergene.size)
        var selectedAllergene = ArrayList<Int>()

        imageViewALLERGIE.setOnClickListener {
            val mBuilder = AlertDialog.Builder(this)
            mBuilder.setTitle("Quelles sont tes allergies ?")
            mBuilder.setMultiChoiceItems(
                allergene,
                checkedAllergene,
                DialogInterface.OnMultiChoiceClickListener { dialog, which, isChecked ->
                    if (isChecked) {
                        if (!selectedAllergene.contains(which)) {
                            selectedAllergene.add(which)
                        } else {
                            selectedAllergene.remove(which)
                        }
                    }
                })

            mBuilder.setCancelable(false)
            mBuilder.setPositiveButton("Valider", DialogInterface.OnClickListener { dialog, which ->
                var items = ""
                var i = 0
                while (i < selectedAllergene.size) {
                    items += allergene[selectedAllergene[i]]
                    if (i != selectedAllergene.size - 1) {
                        items += ", "
                    }
                    i++
                }
                textViewLIST.text = items
            })

            mBuilder.setNegativeButton("Annuler", DialogInterface.OnClickListener { dialog, which ->
                dialog.dismiss()
            })

            mBuilder.setNeutralButton("Tout retirer", DialogInterface.OnClickListener { dialog, which ->
                var i = 0
                while (i < checkedAllergene.size) {
                    checkedAllergene[i] = false
                    selectedAllergene.clear()
                    textViewLIST.text = ""

                    i++
                }
            })

            val mDialog = mBuilder.create()
            mDialog.show()

        }

        // get reference to ImageView
        val buttonINSCRIPTION = findViewById<Button>(R.id.saveInscription)
        // set on-click listener
        buttonINSCRIPTION.setOnClickListener {

            val editTextPRENOM = findViewById<EditText>(R.id.editTextPRENOM)
            val editTextNOM = findViewById<EditText>(R.id.editTextNOM)
            val editTextEMAIL = findViewById<EditText>(R.id.editTextEMAIL)
            val editTextPHONE = findViewById<EditText>(R.id.editTextPHONE)
            val textViewLIST = findViewById<TextView>(R.id.textViewLIST)
            val textViewMDP = findViewById<EditText>(R.id.editTextMDP)

            if ( !editTextPRENOM.text.isBlank()
                && !editTextNOM.text.isBlank()
                && !editTextEMAIL.text.isBlank()
                && !editTextPHONE.text.isBlank()
                && !textViewLIST.text.isBlank() ) {

                var datas = ArrayList<ArrayList<String>>()

                val login = ArrayList<String>()
                login.add("login")
                login.add(editTextEMAIL.text.toString())
                val prenom = ArrayList<String>()
                prenom.add("firstname")
                prenom.add(editTextPRENOM.text.toString())
                val nom = ArrayList<String>()
                nom.add("lastname")
                nom.add(editTextNOM.text.toString())
                val email = ArrayList<String>()
                email.add("email")
                email.add(editTextEMAIL.text.toString())
//                val allergies = ArrayList<String>()
//                allergies.add("allergies")
//                allergies.add(textViewLIST.text.toString())
                val mdp = ArrayList<String>()
                mdp.add("password")
                mdp.add(textViewMDP.text.toString())

                datas.add(login)
                datas.add(prenom)
                datas.add(nom)
                datas.add(email)
//                datas.add(allergies)
                datas.add(mdp)
                Log.d("test", datas.toString())

                val client = Client(this, "https://api.partagetesco.fr/v-latest")
                if(client.checkNetworkConnection()) {
                    var result: String?
                    var error: String?
                    var token: String?
                    var context = this
                    lifecycleScope.launch {
                        result = client.httpRequest("POST", "/user", datas)
                        Log.d("RESULT", result)

                        val json = client.parse(result!!)
                        if (json != null) {

                            if (!json.isNull("data")) {

                                val intent = Intent(context, Login::class.java)
                                startActivity(intent)
                            }
                        }
                    }
                }
            }
        }
    }
}
