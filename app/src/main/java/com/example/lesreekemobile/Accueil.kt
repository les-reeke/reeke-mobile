package com.example.lesreekemobile

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class Accueil : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_accueil)

        val btnChercher: Button = findViewById(R.id.btnSearch)
        btnChercher.setOnClickListener {
            val intentMap = Intent(this, Map::class.java)
            startActivity(intentMap)
        }
    }
}
