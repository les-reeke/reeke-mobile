package com.example.lesreekemobile

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView

class MyAddress : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_address)

        // get reference to ImageView
        val back = findViewById<ImageView>(R.id.Back)
        // set on-click listener
        back.setOnClickListener {
            val intent = Intent(this, MyAccount::class.java)
            startActivity(intent)
        }

        // get reference to ImageView
        val newAdr = findViewById<ImageView>(R.id.new_address)
        // set on-click listener
        newAdr.setOnClickListener {
            val intent = Intent(this, NewAddress::class.java)
            startActivity(intent)
        }
    }
}