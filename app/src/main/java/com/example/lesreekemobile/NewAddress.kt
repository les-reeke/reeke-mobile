package com.example.lesreekemobile

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView

class NewAddress : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_address)

        // get reference to ImageView
        val back = findViewById<ImageView>(R.id.Back)
        // set on-click listener
        back.setOnClickListener {
            val intent = Intent(this, MyAccount::class.java)
            startActivity(intent)
        }
    }
}