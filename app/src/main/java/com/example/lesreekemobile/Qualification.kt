package com.example.lesreekemobile

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView

class Qualification : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qualification)

        // get reference to ImageView
        val BackRestau = findViewById<ImageView>(R.id.BackRestau)
        // set on-click listener
        BackRestau.setOnClickListener {
            val intent = Intent(this, Restaurant::class.java)
            startActivity(intent)
        }

        // get reference to ImageView
        val btnAddDish = findViewById<Button>(R.id.btnAddDish)
        // set on-click listener
        btnAddDish.setOnClickListener {
            val intent = Intent(this, AddDish::class.java)
            startActivity(intent)
        }

        // get reference to ImageView
        val btnAddReview = findViewById<Button>(R.id.btnAddReview)
        // set on-click listener
        btnAddReview.setOnClickListener {
            val intent = Intent(this, AddReview::class.java)
            startActivity(intent)
        }
    }
}
