package com.example.lesreekemobile

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import com.example.lesreekemobile.`class`.Restau
import kotlinx.coroutines.launch
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONException
import org.json.JSONObject
import org.osmdroid.api.IMapController
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.ItemizedIconOverlay
import org.osmdroid.views.overlay.OverlayItem
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay
import java.net.URL


class Map : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    var intentRestaurant : Intent? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val ctx = applicationContext
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx))

        setContentView(R.layout.activity_map)

        val map : MapView = findViewById(R.id.map)
        map.setTileSource(TileSourceFactory.MAPNIK)
        map.setBuiltInZoomControls(true)
        map.setMultiTouchControls(true)

        val mapController : IMapController = map.controller
        mapController.setZoom(19.0)

        //checkPermission(Manifest.permission.ACCESS_FINE_LOCATION)
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),1);

    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            val mGpsMyLocationProvider = GpsMyLocationProvider(ctx)
            val mLocationOverlay = MyLocationNewOverlay(mGpsMyLocationProvider, map)
            mLocationOverlay.enableMyLocation()
            mLocationOverlay.enableFollowLocation()
            map.getOverlays().add(mLocationOverlay)
        }

        val position_array = arrayOf("Position Actuelle","Chez moi","Opc2")

        val spinner: Spinner = findViewById(R.id.spinner)
        // Create an ArrayAdapter using a simple spinner layout and languages array
        val aa = ArrayAdapter(this, R.layout.spinner_item, position_array)
        // Set layout to use when the list of choices appear
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        // Set Adapter to Spinner
        spinner.setAdapter(aa)
        var result: String?
        var json: JSONObject?
        val client = Client(this, "https://api.partagetesco.fr/v-latest")
        if (client.checkNetworkConnection()) {
            intentRestaurant = Intent(applicationContext, Restaurant::class.java)
            lifecycleScope.launch {
                var result: String?

                result = client.httpRequest("GET", "/restaurants")
                    Log.d("Result",result)
                    val json = client.parse(result)
                    val jsonArray = json!!.getJSONArray("data")
                    val items = ArrayList<OverlayItem>()
                    for (i in 0 until jsonArray!!.length()) {
                        val jsonObject = jsonArray.getJSONObject(i).getJSONObject("attributes")
                        items.add(
                            OverlayItem(
                                jsonObject.getString("name"),
                                "description",
                                GeoPoint(
                                    jsonObject.getDouble("latitude"),
                                    jsonObject.getDouble("longitude")
                                )
                            )
                        )
                        val test = Restau(jsonObject.getString("name"),
                                jsonObject.getString("country"),
                                jsonObject.getString("tel"),
                                jsonObject.getString("mail"),
                                jsonObject.getString("address"),
                                jsonObject.getDouble("latitude"),
                                jsonObject.getDouble("longitude"))
                        intentRestaurant!!.putExtra(test.name,test)
                }
                val itemizedIconOverlay = ItemizedIconOverlay(items, osmRestauListener, applicationContext)
                map.overlays.add(itemizedIconOverlay)
            }
        }
    }

    fun parse(json: String): JSONObject? {
        var jsonObject: JSONObject? = null
        try {
            jsonObject = JSONObject(json)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        return jsonObject
    }
    override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {

    }

    override fun onNothingSelected(parent: AdapterView<*>) {

    }

    private val osmRestauListener = object : ItemizedIconOverlay.OnItemGestureListener<OverlayItem> {
        override fun onItemSingleTapUp(index: Int, item: OverlayItem?): Boolean {
            intentRestaurant!!.putExtra("name", item?.title)
            startActivity(intentRestaurant)
            return true
        }

        override fun onItemLongPress(index: Int, item: OverlayItem?): Boolean {
            TODO("Not yet implemented")
            return true
        }
    }

}

