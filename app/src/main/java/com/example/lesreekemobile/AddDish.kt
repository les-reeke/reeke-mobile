package com.example.lesreekemobile

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView

class AddDish : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_dish)

        // get reference to ImageView
        val BackQualif = findViewById<ImageView>(R.id.BackQualif)
        // set on-click listener
        BackQualif.setOnClickListener {
            val intent = Intent(this, Qualification::class.java)
            startActivity(intent)
        }
    }
}
