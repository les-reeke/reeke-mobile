package com.example.lesreekemobile

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.launch

class MyAccount : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_account)

        var str: String? = ""
        if (intent != null){
            // Le code pour récupérer les extras ira ici
            if (intent.hasExtra("token")) { // vérifie qu'une valeur est associée à la clé “edittext”
                str = intent.getStringExtra("token") // on récupère la valeur associée à la clé
            }
        }

        // get reference to ImageView
        val btnInfo = findViewById<Button>(R.id.MesInfos)
        // set on-click listener
        btnInfo.setOnClickListener {
            val intent = Intent(this, MyInformations::class.java)
            startActivity(intent)
        }

        // get reference to ImageView
        val btnAdr = findViewById<Button>(R.id.mesAdresses)
        // set on-click listener
        btnAdr.setOnClickListener {
            val intent = Intent(this, MyAddress::class.java)
            startActivity(intent)
        }

        // get reference to ImageView
        val btnParameters = findViewById<Button>(R.id.Parametres)
        // set on-click listener
        btnParameters.setOnClickListener {
            val intent = Intent(this, Parameters::class.java)
            startActivity(intent)
        }
    }
}